package com.btb.springwebmvc.service.imp;

import com.btb.springwebmvc.repository.model.Student;
import com.btb.springwebmvc.repository.StudentRepository;
import com.btb.springwebmvc.service.StudentService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentServiceImp implements StudentService {

  private final StudentRepository studentRepository;

  @Autowired
  public StudentServiceImp(StudentRepository studentRepository) {
    this.studentRepository = studentRepository;
  }

  @Override
  public List<Student> findAll() {
    return studentRepository.findAll();
  }

  @Override
  public Student findOne(int id) {
    return studentRepository.findOne(id);
  }

  @Override
  public void addNew(Student student) {
studentRepository.addNew(student);
  }
}
