package com.btb.springwebmvc.service;

import com.btb.springwebmvc.repository.model.Student;
import java.util.List;

public interface StudentService {
  List<Student> findAll();
  public Student findOne(int id);
  public void addNew(Student student);

}
