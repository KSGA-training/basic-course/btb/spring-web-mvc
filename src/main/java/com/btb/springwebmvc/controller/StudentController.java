package com.btb.springwebmvc.controller;

import com.btb.springwebmvc.repository.model.Student;
import com.btb.springwebmvc.service.StudentService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/students")
public class StudentController {

  private final StudentService studentService;

  @Autowired
  public StudentController(StudentService studentService) {
    this.studentService = studentService;
  }

  @GetMapping
  public String showAllStudent(ModelMap modelMap){
    modelMap.addAttribute("studentList",studentService.findAll());
    modelMap.addAttribute("msg","KSGA-Spring MVC Class");
    return "index";
  }

@GetMapping("/register")
  public String showFormAdd(){


    return "form-register";
}



@PostMapping("/add")
  public String addStudent(@ModelAttribute @Valid Student student, BindingResult result){
    if(result.hasErrors()){
      System.out.println(result.getFieldError().toString());
      return "redirect:/students/register";
    }

   List<Student> studentList = studentService.findAll();
   student.setId(studentList.size()+1);
    studentService.addNew(student);
    return "redirect:/students";
}





}
