package com.btb.springwebmvc.controller.restcontroller;

import com.btb.springwebmvc.repository.model.Student;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/students")
public class StudentRestController {

  List<Student> studentList = new ArrayList<>();
  {
    studentList.add(new Student(1,"Kimhor","Male"));
    studentList.add(new Student(2,"Kimchhor","Female"));
    studentList.add(new Student(3,"Jork","Male"));
  }

  @GetMapping("/findAll")
  public List<Student> getAll(){
    return studentList;
  }

  @GetMapping("/welcome")
  public String welcomeMsg(){
    Student student = new Student(1,"Porleas","Male");
    return "Welcome to Spring Class";
  }

@GetMapping("/find-by-id/{id}")
  public Student findOne(@PathVariable  int id){
    Student student = studentList.get(id-1);
    return student;

}
@GetMapping("/search")
  public List<Student> findByGender(@RequestParam(name = "g") String gender){
    List<Student> result = studentList.stream().
        filter(student -> student.getGender().equalsIgnoreCase(gender)).collect(Collectors.toList());
    return result;
}

}
