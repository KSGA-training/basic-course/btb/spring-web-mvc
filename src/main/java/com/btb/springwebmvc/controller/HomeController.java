package com.btb.springwebmvc.controller;

import com.btb.springwebmvc.repository.model.Student;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

  @GetMapping({"","/index","/home"})
  public String viewIndex(ModelMap modelMap){
    String message = "Welcome KSGA";
    Student student = new Student(1,"Kimhor","Male");
    modelMap.addAttribute("s",student);
    modelMap.addAttribute("msg",message);

    return "index";
  }




  @GetMapping("/register")
  public String getRegisterForm(){
    return "form-register";
  }

}
