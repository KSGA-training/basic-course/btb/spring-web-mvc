package com.btb.springwebmvc.repository.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student {
  private int id;
  @NotEmpty(message = "name can't be empty")
  @NotNull
  private String name;

  @NotEmpty(message = "gender can't be empty")
  @NotNull(message = "gender can't be null")
  private String gender;


}
