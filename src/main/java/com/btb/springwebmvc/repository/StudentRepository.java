package com.btb.springwebmvc.repository;

import com.btb.springwebmvc.repository.model.Student;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;

@Repository
public class StudentRepository {
  List<Student> studentList = new ArrayList<>();
  {
    studentList.add(new Student(1,"Kimhor","Male"));
    studentList.add(new Student(2,"Kimchhor","Female"));
    studentList.add(new Student(3,"Jork","Male"));
  }


  //TODO: get all students
  public List<Student>  findAll(){
    return studentList;
}


  //TODO: get a student by id
  public Student findOne(int id)
  {
    Student student = studentList.get(id-1);
    return student;
  }


   public void addNew(Student student){
    studentList.add(student);
   }

}
